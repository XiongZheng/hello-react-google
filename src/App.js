import React from 'react';
import './App.less';
import Menu from "./Menu";
import Search from './Search';
import Footer from './Footer';

const App = () => {
  return (
    <div className='App'>
      <Menu />
      <Search buttonTest1="Hello" buttonTest2="World" />
      <Footer/>
    </div>
  );
};

export default App;