import React from 'react'

export const Button = props=>{
  const  {text}= props
    return <button className="button">{text}</button>
}