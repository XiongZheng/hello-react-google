import React from 'react';
import './Footer.less'

const Footer=()=>{
  return (
    <footer className='footer'>
      <nav className='left-nav'>
        <a>广告</a>
        <a>商务</a>
        <a>Google大全</a>
        <a>Google搜索的运作方式</a>
      </nav>
      <nav className='right-nav'>
        <a>隐私权</a>
        <a>条款</a>
        <a>设置</a>
      </nav>
    </footer>
  )
}
export default Footer;