import React from 'react';
import './Search.less';
import {Button} from "./Button";

const Search = (props) => {
  const {buttonTest1,buttonTest2}=props;
  return (
    <section className='search'>
      <header>
        <img src="https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png"
                   alt="Google Logo"/>
      </header>
      <input type='text'></input>
      <Button text={buttonTest1}/>
      <Button text={buttonTest2}/>
    </section>
  );
};

export default Search;